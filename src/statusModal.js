import React from 'react'

export default class StatusModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <section id="status" className="ninthSectionBg">
                <div>
                    <div>
                        <div className="col-12 phaseFirst">
                            <div style={{ float: 'left' }} className="phaseHead">Phase 1</div>
                            <div style={{ float: 'right' }} className="phaseHead">Status : Done </div>
                        </div>
                        <div className="phaseBox">
                            <div className="phaseBoxText">  The current version, is a fully functioning easy to implement monitoring and alerting solution,
                            can scale easily and adapt to multiple sites and multiple customers at any given point of time
                            ( currently available )
                                </div>
                        </div>
                    </div>
                    <div className="col-12 phaseFirst" style={{ marginTop: '50px' }}>
                        <div style={{ float: 'left' }} className="phaseHead">Phase 2</div>
                        <div style={{ float: 'right' }} className="phaseHead">Status : In Progress</div>
                    </div>
                    <div>
                        <div className="phaseBox">
                            <div className="phaseBoxText"> The next version will have support on handling seamless workflows related to maintenance /
                            operations team, along with metrics/insights that can be customised for respective business
                            needs . Includes knowledge base builder and management of devices remotely.
                                </div>
                        </div>
                    </div>
                    <div>
                        <div className="col-12 phaseFirst" style={{ marginTop: '50px' }}>
                            <div style={{ float: 'left' }} className="phaseHead">Phase 3</div>
                            <div style={{ float: 'right' }} className="phaseHead">Status : TBD </div>
                        </div>
                        <div className="phaseBox">
                            <div className="phaseBoxText">OTA support for updates. Build your own Dashboard facility that can give any non tech person
                            an ability to configure and run his business easily. This will enable the no-code type of ability
                            to setup and configure solutions in connected device / IOT/ IIOT space
                                </div>
                        </div>
                    </div>
                    {/* <div style={{ textAlign: 'center' }}>
                            <button className="lastBut">Subscribe for updates on progress</button>
                        </div> */}
                </div>
            </section>
        )
    }
}