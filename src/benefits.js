import React from 'react'
import BenefitsFirst from './photos/benefitsFirst.png';
import BenefitsSecond from './photos/benefitsSecond.png';
import Benefitsthird from './photos/benefitsthird.png';
import BenefitsFourth from './photos/benefitsFourth.png';
import BenefitsFIveth from './photos/benefitsFIveth.png';
import BenefitsSixth from './photos/benefitsSixth.png';
import BenefitsSeventh from './photos/benefitsSeventh.png';
import BenefitsEighth from './photos/benefitsEighth.png';

export default class Benefits extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <div>
                <div className="row" style={{ marginBottom: '15px' }}>
                    <div className="col-md-12 padding-remove">
                        <div style={{ flex: '1', textAlign: 'center' }} className="headingofModal">
                            <span className="benefitsHeadCss">Benefits
                            <i class="fas fa-times ml-1" style={{ color: 'gray', float: 'right', paddingBottom: '10px', paddingRight: '10px' }} onClick={()=>this.props.toggleCancel()}></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="benefitsCss row">
                            <div className="col-2">
                                <img src={BenefitsFirst} className="benefitsImg" />
                            </div>
                            <div className="col-10">
                                Save Time, Plug n Play dashboard/workflow/
                                analytics setup
                         </div>
                        </div>
                        <div className="benefitsCss row">
                            <div className="col-2">
                                <img src={BenefitsSecond} className="benefitsImg" />
                            </div>
                            <div className="col-10">
                                Fix leaky areas by data insights provided
                        </div>
                        </div>
                        <div className="benefitsCss row">
                            <div className="col-2">
                                <img src={Benefitsthird} className="benefitsImg" />
                            </div>
                            <div className="col-10">
                                Get insights in a prescription format
                        </div>
                        </div>
                        <div className="benefitsCss row">
                            <div className="col-2">
                                <img src={BenefitsFourth} className="benefitsImg" />
                            </div>
                            <div className="col-10">
                                Adjust actions/flows as per needs
                          </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="benefitsCss row">
                            <div className="col-2">
                                <img src={BenefitsFIveth} className="benefitsImg" />
                            </div>
                            <div className="col-10">
                                Track expense, fixes needed and improve
                    </div>
                        </div>
                        <div className="benefitsCss row">
                            <div className="col-2">
                                <img src={BenefitsSixth} className="benefitsImg" />
                            </div>
                            <div className="col-10">
                                Planned itinerary to team members that saves time
                    </div>
                        </div>
                        <div className="benefitsCss row">
                            <div className="col-2">
                                <img src={BenefitsSeventh} className="benefitsImg" />
                            </div>
                            <div className="col-10">
                                Easily translate data into money lingo, to
                                understand expense and savings
                    </div>
                        </div>
                        <div className="benefitsCss row">
                            <div className="col-2">
                                <img src={BenefitsEighth} className="benefitsImg" />
                            </div>
                            <div className="col-10">
                                Assist in finding the right hardware/device/
                                gateway/logger/sensor partner
                              </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}