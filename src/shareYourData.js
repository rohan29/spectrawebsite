import React from 'react'
import './css/firstPage.css'

export default class ShareYourData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        const { firstCard, secondCard, firstPeople, faqModal, benefitsModal, shareYourDataModal, thirdPeople, videoModal, namefirst, emailfirst, phoneNofirst, requrmentfirst, configureModal, getBack, usertabIndex } = this.state;
        return (
            <div >
                <div style={{ display: 'flow-root' }}>
                    <i class="fas fa-times ml-1" style={{ color: 'gray', float: 'right', paddingBottom: '10px' }} onClick={() => this.props.toggleCancel(false)}></i>
                </div>
                <form id="contact" style={{ width: 'auto', boxShadow: 'none', padding: '0px' }} onSubmit={this.firstForm}  >
                    <fieldset style={{ display: 'none' }}>
                        <input name="subject" placeholder="subject" subject value="Enquiry from Device/Equipment manufacturers" type="text" />
                    </fieldset>
                    <fieldset>
                        <input name="namefirst" placeholder="Customer Name" onChange={(e) => this.inputChangeHandler(e)} value={namefirst} type="text" />
                    </fieldset>
                    <fieldset>
                        <input placeholder="Contact Number" name="phoneNofirst" type="tel" onChange={(e) => this.inputChangeHandler(e)} value={phoneNofirst} />
                    </fieldset>
                    <fieldset>
                        <input required name="emailfirst" placeholder="Email ID" onChange={(e) => this.inputChangeHandler(e)} value={emailfirst} type="email" />
                    </fieldset>
                    <fieldset >
                        <input name="domain" placeholder="Domain being served" onChange={(e) => this.inputChangeHandler(e)} value={namefirst} type="text" />
                    </fieldset>
                    <fieldset>
                        <textarea placeholder="Explain the needs , looking forward to work together. " name="requrmentfirst" onChange={(e) => this.inputChangeHandler(e)} value={requrmentfirst}></textarea>
                    </fieldset>
                    <button name="submit" type="submit" style={{ background: '#2d7c47' }}>Submit</button>
                </form>
            </div>
        )
    }
}