import React from 'react';
import './css/firstPage.css'
import FromHere from './photos/fromHere.jpg';
import ToHere from './photos/toHere.jpg';
import FirstImg from './photos/firstImg.jpg';
import SecondImg from './photos/secondImg.jpg';
import ThiredImg from './photos/thiredImg.jpg';
import Dashboard from './photos/dashboard.png';
import DashboardDown from './photos/dashboardDown.png';
import Alerts from './photos/alerts.png';
import AlertsDown from './photos/alertsDown.png';
import Customer from './photos/customer.png';
import CustomerDown from './photos/customerDown.png';
import MilkChiller from './photos/milkchiller.jpeg';
import ColdStorageRoom from './photos/coldstorage_rooms.png';
import Refrigerators from './photos/refrigerators.jpg';
import ReeferFirst from './photos/reeferFirst.jpeg';
import ReeferSecond from './photos/reeferSceond.png';
import PrometheanLogo from './photos/prometheanLogo.png';
import MultiflexLogo from './photos/multiflexLogo.png';
import NavBar from './navBar';
import DashBoard from './dashBoard';
import { Modal, ModalBody } from 'reactstrap';
import VideoModal from './videoModal';
import emailjs from 'emailjs-com';
import FaqSection from './faqSwction';
import Benefits from './benefits';
import ShareYourData from './shareYourData';
import FirstPageRight from './photos/firstPageRight.png';
import StatusModal from './statusModal';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Reefer from './photos/reefer.jpg';
import MilkPoint from './photos/milkPoint.svg';
import ColdPoint from './photos/coldPoint.svg';
import ReeferPoint from './photos/reeferPoint.svg';
// import MilkChiller from './photos/milkChiller.jpg';
import ColdStorage from './photos/coldstorage.jpg';
export default class FirstPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            videoModal: false,
            firstCard: true,
            secondCard: false,
            faqModal: false,
            benefitsModal: false,
            statusModa: false,
            usertabIndex: 0
        }
    }
    videoModalHandle = () => {
        this.setState({
            videoModal: !this.state.videoModal,
        })
    }
    firstForm = (e) => {
        // const { namefirst, emailfirst, phoneNofirst, requrmentfirst } = this.state;
        e.preventDefault();

        emailjs.sendForm('gmail', 'template_en2wtgm', e.target, 'user_faJmTGlsJR8vmkONPAbWB')
            .then((result) => {
                this.setState({
                    getBack: true,
                    namefirst: '',
                    emailfirst: '',
                    phoneNofirst: "",
                    requrmentfirst: ''
                })
                setTimeout(() => {
                    this.setState({
                        getBack: false
                    })
                }, 5000);
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
    }
    cardHoverShow = (e) => {
        const remainingClickFalse = {
            firstCard: false,
            secondCard: false
        }
        this.setState({
            ...remainingClickFalse, [e.target.id]: true,
            getBack: false
        })
    }
    faqModalHandle = (e) => {
        this.setState({
            faqModal: !this.state.faqModal
        })
    }
    benefitsModalHandle = (e) => {
        this.setState({
            benefitsModal: !this.state.benefitsModal
        })
    }
    shareYourDataModa = (e) => {
        this.setState({
            shareYourDataModal: !this.state.shareYourDataModal
        })
    }
    statusModal = (e) => {
        this.setState({
            statusModa: !this.state.statusModa
        })
    }
    render() {
        const { firstCard, secondCard, firstPeople, statusModa, faqModal, benefitsModal, shareYourDataModal, thirdPeople, videoModal, namefirst, emailfirst, phoneNofirst, requrmentfirst, configureModal, getBack, usertabIndex } = this.state;
        return (
            <div>
                <Modal className="modal-width" isOpen={videoModal} toggle={this.videoModalHandle} >
                    <ModalBody style={{ borderRadius: '20px' }} className="assignModalBody">
                        <VideoModal toggleCancel={this.videoModalHandle} />
                    </ModalBody>
                </Modal>
                <Modal className="modal-width" isOpen={faqModal} toggle={this.faqModalHandle} >
                    <ModalBody style={{ borderRadius: '20px' }} className="assignModalBody">
                        <FaqSection toggleCancel={this.faqModalHandle} />
                    </ModalBody>
                </Modal>
                <Modal className="modal-widthBig" isOpen={benefitsModal} toggle={this.benefitsModalHandle} >
                    <ModalBody style={{ borderRadius: '20px' }} className="assignModalBody">
                        <Benefits toggleCancel={this.benefitsModalHandle} />
                    </ModalBody>
                </Modal>
                <Modal className="modal-width" isOpen={shareYourDataModal} toggle={this.shareYourDataModa} >
                    <ModalBody style={{ borderRadius: '20px' }} className="assignModalBody">
                        <ShareYourData toggleCancel={this.shareYourDataModa} />
                    </ModalBody>
                </Modal>
                <Modal className="modal-widthBig" isOpen={statusModa} toggle={this.statusModal} >
                    <ModalBody style={{ borderRadius: '20px' }} className="assignModalBody">
                        <StatusModal toggleCancel={this.statusModal} />
                    </ModalBody>
                </Modal>
                <NavBar />
                <section className="firstSectionBg">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <p className="firstSectionText">
                                    {/* Automate remote monitoring and management of machines/appliances, support your team with relevant contextual data in real time. */}
                                    Simple plug n play solution to monitor your machines remotely in real time, send alerts to the right team members
                                   <br /><span>
                                        <span className="firstSectionTextSub"> Easy to use, scales easily, save time.</span>
                                    </span>
                                </p>
                                <div className="flex webButHideShow">
                                    <button className="watchVideo  mr-4">
                                        <span className="firstSectionLink" onClick={() => this.videoModalHandle(true)}>Watch Video
                                        <i class="fas fa-play ml-1" style={{ color: '#225aff' }}></i>
                                        </span>
                                    </button>
                                    <button className="idealCustomer">
                                        <a href="#idealCustomer"> <span className="firstSectionLink" style={{ color: '#fff' }}> Ideal Customer</span></a>
                                    </button>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="marginCenter">
                                    <div className="row" style={{ float: 'right' }}>
                                        {/* <div className="col-5" style={{ textAlign: 'end' }}>
                                            <img src={FromHere} className="firstSectionImg" alt="FromHere" />
                                            <p className="firstSectionImgText">From Here</p>
                                        </div>
                                        <div className="col-2 marginMid" >
                                            <span className="infew">In Few</span>
                                            <br />
                                            --->
                                            <br />
                                            <span className="infew">Clicks</span>
                                        </div>
                                        <div className="col-5">
                                            <img src={ToHere} className="firstSectionImg" alt="ToHere" />
                                            <p className="firstSectionImgText"> To Here</p>
                                        </div> */}
                                        <img src={FirstPageRight} />
                                    </div>
                                </div>
                                <div className="flex moblieButHideShow">
                                    <button className="watchVideo  mr-4">
                                        <span className="firstSectionLink" onClick={() => this.videoModalHandle(true)}>Watch Video
                                        <i class="fas fa-play ml-1" style={{ color: '#225aff' }}></i>
                                        </span>
                                    </button>
                                    <button className="idealCustomer">
                                        <a href="#idealCustomer"> <span className="firstSectionLink" style={{ color: '#fff' }}> Ideal Customer</span></a>
                                    </button>
                                </div>
                                {/* <div>
                                    <p className="firstSectionImgbelowText">In Just Few Clicks</p>
                                </div> */}
                                {/* <div>
                                    <p className="firstSectionImgbelowbox">Let your teams work in harmony and calmness, by looking at the right data everytime</p>
                                </div> */}
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <p className="firstSectionImgbelowbox">
                                    Let your teams work in harmony and calmness, by looking at the right data everytime
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="firstSectionSubBg">
                    <div className="container">
                        <div className="row">
                            {/* <div className="col-12">
                                <p className="firstSectionImgbelowbox">
  Let your teams work in harmony and calmness, by looking at the right data everytime
                                </p>
                            </div> */}
                        </div>
                    </div>
                </section>
                <section className="secondSectionBg">
                    <div className="container">
                        {/* <div className="row">
                            <div className="col-md-6">
                                <p className="secondSectionText">
                                    Build and setup Dashboards, Workflows and
                                    Analytics in few clicks.
                                   <br />  <br /><span >
                                        Automate every flow, save your time
                                    </span>
                                </p>
                            </div>
                            <div className="col-md-6">
                                <img src={Dashboard} className="secondSectionDashboardImg" alt="FromHere" />
                                <img src={DashboardDown} className="secondSectionDashboardDownImg" alt="FromHere" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <img src={Alerts} className="thirdectionDashboardImg" alt="FromHere" />
                                <img src={AlertsDown} className="thirdSectionDashboardDownImg" alt="FromHere" />
                            </div>
                            <div className="col-md-6">
                                <p className="thirdSectionText">
                                    Real time Alerts that can be easily configured to help team members take necessary action.
                                </p>
                            </div>
                        </div>*/}
                        {/* <div className="row">
                            <div className="col-md-6">
                                <p className="fourthSectionText">
                                    Easy to setup teams, customer
                                    teams, sites to be monitored
                                    within few minutes
                                </p>
                            </div>
                            <div className="col-md-6">
                                <img src={Customer} className="fourthSectionDashboardImg" alt="FromHere" />
                                <img src={CustomerDown} className="fourthSectionDashboardDownImg" alt="FromHere" />
                            </div>
                        </div>  */}
                        <div className="row">
                            <div className="col-md-6 marginMid">
                                <p className="firstImgText" >
                                    Build and setup Dashboards, Workflows and
                                    Analytics in few clicks. Automate every flow, save your time
                                </p>
                            </div>
                            <div className="col-md-6">
                                <img src={FirstImg} className="firstImg" alt="FromHere" />
                            </div>
                        </div>
                        <div className="row" style={{ marginTop: '100px' }}>
                            <div className="col-md-6">
                                <img src={SecondImg} className="secondImg" alt="FromHere" />
                            </div>
                            <div className="col-md-6 marginMid" >
                                <p className="secondImgText">
                                    Real time Alerts that can be easily configured to help team members take necessary action.
                                </p>
                            </div>
                        </div>
                        <div className="row heighAdjest">
                            <div className="col-md-6 marginMid">
                                <p className="thiredImgText">
                                    Easy to setup teams, customer
                                    teams, sites to be monitored
                                    within few minutes
                                </p>
                            </div>
                            <div className="col-md-6">
                                <img src={ThiredImg} className="thiredImg" alt="FromHere" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div style={{ textAlign: 'center' }} onClick={this.benefitsModalHandle}>
                                    <button className="but cardsBut">Benefits</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="secondSectionSubBg">
                    <div className="container">
                        <div className="row">
                            {/* <div className="col-12">
                                <div style={{ textAlign: 'center' }} onClick={this.benefitsModalHandle}>
                                    <button className="but cardsBut">Benefits</button>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </section>
                <section id="feature" className="thirdSectionBg">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 eightHeadTag">
                                <span className="PricingHead">Features</span>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="cardBox">
                                    <div className="cardBoxHead">Bring your own Device</div>
                                    <div className="cardBoxSubHead"> - Device Agnostic, adapts as needed </div>
                                    <div className="cardBoxSubHead"> - Plug n Play mode, ready to go </div>
                                    <div className="cardBoxSubHead"> - Easily configurable and setup </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="cardBox">
                                    <div className="cardBoxHead">Buid your own Dashboard</div>
                                    <div className="cardBoxSubHead"> - Setup dashboard as you want</div>
                                    <div className="cardBoxSubHead"> - Save time on development </div>
                                    <div className="cardBoxSubHead"> - Get it aligned to your business</div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="cardBox">
                                    <div className="cardBoxHead">Maintenance </div>
                                    <div className="cardBoxSubHead"> - Complete CRM support for service flow</div>
                                    <div className="cardBoxSubHead"> - Check Machine status on the go </div>
                                    <div className="cardBoxSubHead">  - Get insights the way a service engineer wants </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="cardBox">
                                    <div className="cardBoxHead">Alerts and Notifications</div>
                                    <div className="cardBoxSubHead"> - Real time alert setup made easy </div>
                                    <div className="cardBoxSubHead"> - Team member assignment made easy </div>
                                    <div className="cardBoxSubHead"> - Take care of issue/anamoly early </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="cardBox">
                                    <div className="cardBoxHead">Data Analysis and Patterns</div>
                                    <div className="cardBoxSubHead"> - Find details needed to function well</div>
                                    <div className="cardBoxSubHead"> - Give team members info that works </div>
                                    <div className="cardBoxSubHead"> - Automate once, save time </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="cardBox">
                                    <div className="cardBoxHead">Knowledge Base Builder</div>
                                    <div className="cardBoxSubHead"> - Automatic information builder for resolutions provided </div>
                                    <div className="cardBoxSubHead">   - Service engineer onboarding made easy </div>
                                </div>
                            </div>
                            <div className="col-12">
                                <div style={{ textAlign: 'center' }} onClick={this.shareYourDataModa}>
                                    <button className="but cardsBut"> Ask us if you have a question</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="thirdSectionSubBg">
                    <div className="container">
                        <div className="row">
                        </div>
                    </div>
                </section>
                <section id="useCases" className="fourthSectionBg">
                    <div className="container">
                        {/* <div className="row">
                            <div className="col-md-6 col-12 paddingRightChanges">
                                <div className="milkChillerImgBox">
                                    <img className="milkChillerImg" src={MilkChiller} />
                                </div>
                            </div>
                            <div className="col-md-6 col-12 paddingLeftChanges">
                                <div className="milkChillerImgTextBox">
                                    <p className="milkChillerImgText">
                                        -  Real time alerts and dashboards quick setup
                                        gave teams better visibility of sites in shorter
                                    duration and milk freshness/health<br />
                                        <br />
                                    -  Helped in improving the response times for
                                    attending any issues and avoiding them
                                    much before<br />
                                        <br />
                                    -  Made operations team work more better
                                    and productive with real time information
                                    is accessible anytime, anywhere.<br />
                                        <br />
                                    - Plug n Play mode made it to add more sites
                                    as they scaled easily<br />
                                    </p>
                                    <div style={{ textAlign: 'center' }}>
                                        <button className="leanMoreBut">Learn More</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-md-6 col-12 paddingRightChanges">
                                <div className="coldStorageRoomImgBox">
                                    <div> <img src={Refrigerators} className="coldStorageRoomImg" /></div>
                                    <div> <img src={ColdStorageRoom} className="refrigeratorsRoomImg" /></div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12 paddingLeftChanges">
                                <div className="coldStorageRoomImgText">
                                    <p className="coldStorageRoomText">
                                        -  Parameter setup and monitoring temperature
                                        and door opened or closed status , these use
                                       cases were implemented easily with Spectra.<br />
                                        <br />
                                    -  Power consumption , report generation, issue
                                       tracking - site wise became easy and saved
                                       time for the team members involved<br />
                                        <br />
                                    -  Adding multiple sites and machines is now
                                       handled by the team head, the setup is easy
                                       and can be done by anyone<br />
                                        <br />
                                    -  Analysis and reports proved beneficial for the
                                       teams to spot the caveats in wastage/spoil<br />
                                    </p>
                                    <div style={{ textAlign: 'center' }}>
                                        <button className="leanMoreBut">Learn More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-12 paddingRightChanges">
                                <div className="reeferImgBox">
                                    <div>
                                        <img src={ReeferFirst} className="reeferImgImg" />
                                        <img src={ReeferSecond} className="reeferSecondImgImg" /></div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12 paddingLeftChanges">
                                <div className="reeferImgText">
                                    <p className="reeferfirstImgText">
                                        - Tracking of vehicles feature allowed the teams
                                in planning for activities once delivery happens<br />
                                        <br />
                                    - Adding a reefer into configuration was made
                                easy as an admin could operate as per their
                                needs<br />
                                        <br />
                                    - Using Spectra helped the team to maintain and
                                preserve freshness of dairy/beverage based
                                products<br />
                                        <br />
                                    - Worked well for deliveries from remote places
                                with certain limitations.<br />
                                    </p>
                                    <div style={{ textAlign: 'center' }}>
                                        <button className="leanMoreBut">Learn More</button>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        <div className="userConfiigure">
                            <div className="row">
                                <div className="col-12 eightHeadTag" >
                                    <span className="PricingHead">Use Cases</span>
                                </div>
                            </div>
                            <Tabs id="controlled-tab-example" selectedIndex={usertabIndex} onSelect={usertabIndex => this.setState({ usertabIndex })}>
                                <TabList>
                                    <Tab style={{ fontSize: '16px' }}>Milk Chiller</Tab>
                                    <Tab style={{ fontSize: '16px' }}>Cold Storage</Tab>
                                    <Tab style={{ fontSize: '16px' }}>Reefer</Tab>
                                </TabList>
                                <TabPanel>
                                    <div className="grid-item-config">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="testcolor" style={{ backgroundColor: '#10c6a3' }}>
                                                    <img src={MilkChiller} className="milkChillerImg" alt="milkChiller" />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="milkText">
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={MilkPoint} alt="milkText" className="milkPoint" /></div>
                                                        <div className="clent-config"> Real time alerts and dashboards quick setup
                                                            gave teams better visibility of sites in shorter duration and milk freshness/health </div>
                                                    </div>
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={MilkPoint} alt="milkText" className="milkPoint" /></div>
                                                        <div className="clent-config"> Helped in improving the response times for
                                                            attending any issues and avoiding them much before </div>
                                                    </div>
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={MilkPoint} alt="milkText" className="milkPoint" /></div>
                                                        <div className="clent-config">Made operations team work more better
                                                            and productive with real time information is accessible anytime,anywhere.</div>
                                                    </div>
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={MilkPoint} alt="milkText" className="milkPoint" /></div>
                                                        <div className="clent-config">Plug n Play mode made it to add more sites as they scaled easily </div>
                                                    </div>
                                                    {/* <div style={{ display: 'flex' }}>
                                                            <div className="mt-1"><img src={MilkPoint} alt="milkText" className="milkPoint" /></div>
                                                            <div className="clent-config">Data analysis and robust alert rule setting </div>
                                                        </div> */}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <div className="grid-item-config">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="testcolor" style={{ backgroundColor: '#0e7c60' }}>
                                                    <img src={ColdStorage} alt="ColdStorage" />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="coldText">
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={ColdPoint} alt="ColdStorage" className="milkPoint" /></div>
                                                        <div className="clent-config"> Parameter setup and monitoring temperature
                                                        and door opened or closed status , these use cases were implemented easily with Spectra.</div>
                                                    </div>
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={ColdPoint} alt="ColdStorage" className="milkPoint" /></div>
                                                        <div className="clent-config">Power consumption , report generation, issue
                                                        tracking - site wise became easy and saved time for the teammembers involved</div>
                                                    </div>
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={ColdPoint} alt="ColdStorage" className="milkPoint" /></div>
                                                        <div className="clent-config"> Adding multiple sites and machines is now
                                                        handled by the team head, the setup is easy and can be done by anyone</div>
                                                    </div>
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={ColdPoint} alt="ColdStorage" className="milkPoint" /></div>
                                                        <div className="clent-config">Analysis and reports proved beneficial for the teams to spot the caveats in wastage/spoil</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <div className="grid-item-config">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="testcolor" style={{ backgroundColor: '#117a8b' }}>
                                                    <img src={Reefer} alt="reefer" />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="reeferText">
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={ReeferPoint} alt="reefer" className="milkPoint" /></div>
                                                        <div className="clent-config">Tracking of vehicles feature allowed the teams in planning for activities once delivery happens</div>
                                                    </div>
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={ReeferPoint} alt="reefer" className="milkPoint" /></div>
                                                        <div className="clent-config"> Adding a reefer into configuration was made easy as an admin could operate as per their needs</div>
                                                    </div>
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={ReeferPoint} alt="reefer" className="milkPoint" /></div>
                                                        <div className="clent-config"> Using Spectra helped the team to maintain and preserve freshness of dairy/beverage based products</div>
                                                    </div>
                                                    <div style={{ display: 'flex' }}>
                                                        <div className="mt-1"><img src={ReeferPoint} alt="reefer" className="milkPoint" /></div>
                                                        <div className="clent-config"> Worked well for deliveries from remote places with certain limitations.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                            </Tabs>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div style={{ textAlign: 'center' }} onClick={this.shareYourDataModa}>
                                    <button className="useCaseBut">Share your use case  - lets discuss</button>
                                </div>
                            </div>
                        </div>
                        {/* <div className="row">
                            <div className="col-12 customerCss">
                                Customer
                            </div>
                            <div className="col-md-6 col-12" style={{ textAlign: 'center' }}>
                                <img src={PrometheanLogo} className="customerLogo" />
                            </div>
                            <div className="col-md-6 col-12" style={{ textAlign: 'center' }}>
                                <img src={MultiflexLogo} className="customerLogo" />
                            </div>
                        </div> */}
                    </div>
                </section>
                <section className="fourthSectionSubBg">
                    <div className="container">
                        {/* <div className="row">
                            <div className="col-12">
                                <div style={{ textAlign: 'center' }} onClick={this.shareYourDataModa}>
                                    <button className="useCaseBut">Share your use case  - lets discuss</button>
                                </div>
                            </div>
                        </div> */}
                    </div>
                </section>
                <section id="idealCustomer" className="fifthSectionBg">
                    <div className="colorCoding">
                        <div className="leftColor">
                            <div className="fifthSectionText">
                                <span className="fifththSectionTextHead">
                                    Businesses should be ON everytime,
                                  </span>
                                <span className="fifthSectionTextSub" style={{ marginTop: '0px' }}>
                                    Humans
                                    should be making decisions and not dumb work
                                    everytime, making decision needs relevant data
                                    points. For us an Ideal Customer is someone who
                                    wants to run their operations or maintenance related
                                    activities seamlessly, the team should experience as if a head or a lead is talking to them even in their absence.
                                    </span>
                                {/* <span className="fifthSectionTextSub">Some areas we are focusing to serve are as follows
                                If you have a specifc use case, do contact us.</span> */}
                                <span className="fifthSectionTextSub"> (cold chain, refrigeration, reefer, chillers are some of the areas we have handled till date)</span>
                            </div>
                        </div>
                        <div className="rightColor">
                            <div className="fidthSectionPoints">
                                <div className="row">
                                    <div className="col-6" style={{ textAlign: 'center' }}>
                                        <button className="fidthSectionBut">Warehouse</button>
                                    </div>
                                    <div className=" col-6" style={{ textAlign: 'center' }}>
                                        <button className="fidthSectionBut">Cloud Kitchens</button>
                                    </div>
                                    <div className="col-6" style={{ textAlign: 'center' }}>
                                        <button className="fidthSectionBut">Vending Machines</button>
                                    </div>
                                    <div className="col-6" style={{ textAlign: 'center' }}>
                                        <button className="fidthSectionBut"> Consumer Appliances</button>
                                    </div>
                                    <div className="col-6" style={{ textAlign: 'center' }}>
                                        <button className="fidthSectionBut">Cold Chain/chillers/reefer</button>
                                    </div>
                                    <div className="col-6" style={{ textAlign: 'center' }}>
                                        <button className="fidthSectionBut">Franchine businesses</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="customerBg">
                    <div className="row">
                        <div className="col-12 customerCss" style={{ textDecoration: 'underline' }}>
                            Customers
                            </div>
                        <div className="col-md-6 col-12" style={{ textAlign: 'center' }}>
                            <img src={PrometheanLogo} className="customerLogo" />
                        </div>
                        <div className="col-md-6 col-12" style={{ textAlign: 'center' }}>
                            <img src={MultiflexLogo} className="customerLogo" />
                        </div>
                    </div>
                </section>
                <section className="fifthSectionSubBg">
                    <div className="container">
                        <div className="row">
                            <div className='col-12 ' style={{ textAlign: 'center' }}>

                            </div>
                        </div>
                    </div>
                </section>
                <section id="partner" className="sixthSectionBg">
                    <div className="container">
                        <div className="row">
                            {/* <div className='col-12' style={{ textAlign: 'center' }}>
                                <p className="sixthquote">  "No one can do everything, but everyone can do something,
                            and together we can change the world." ~ Ronald J. Sider</p>
                            </div> */}
                            <div className='col-12' style={{ textAlign: 'center' }}>
                                <p className="sixthSecondquote">If you are a machine/equipment manufacturer or gateway/sensor/logger producer, we would want to partner with you. Please let us know your details below and we will get in touch.</p>
                            </div>
                            <div className="col-md-4 col-12">
                                <div id="firstCard" onClick={this.cardHoverShow}>
                                    <button id="firstCard" className="sixthButOne" style={{ boxShadow: firstCard ? '7px 7px 7px rgb(0 0 0 / 85%)' : '0px 0px 0px rgb(0 0 0 / 35%)' }}>
                                        Machine/Equipment
                                </button>
                                </div>
                                <div id="secondCard" onClick={this.cardHoverShow}>
                                    <button id="secondCard" className="sixthButTwo" style={{ boxShadow: secondCard ? '7px 7px 7px rgb(0 0 0 / 85%)' : '0px 0px 0px rgb(0 0 0 / 35%)' }}>
                                        Gateway/Logger/<br />
                                    Sensor
                                </button>
                                </div>
                            </div>
                            <div className="col-md-8 col-12">
                                <div>
                                    <form id="contact" onSubmit={this.firstForm} style={{ display: firstCard ? 'block' : 'none' }} >
                                        <fieldset style={{ display: 'none' }}>
                                            <input name="subject" placeholder="subject" subject value="Enquiry from Device/Equipment manufacturers" type="text" />
                                        </fieldset>
                                        <fieldset>
                                            <input name="namefirst" placeholder="Customer Name" onChange={(e) => this.inputChangeHandler(e)} value={namefirst} type="text" />
                                        </fieldset>
                                        <fieldset>
                                            <input placeholder="Contact Number" name="phoneNofirst" type="tel" onChange={(e) => this.inputChangeHandler(e)} value={phoneNofirst} />
                                        </fieldset>
                                        <fieldset>
                                            <input required name="emailfirst" placeholder="Email ID" onChange={(e) => this.inputChangeHandler(e)} value={emailfirst} type="email" />
                                        </fieldset>
                                        <fieldset >
                                            <input name="domain" placeholder="Domain being served" onChange={(e) => this.inputChangeHandler(e)} value={namefirst} type="text" />
                                        </fieldset>
                                        <fieldset>
                                            <textarea placeholder="Explain the needs , looking forward to work together. " name="requrmentfirst" onChange={(e) => this.inputChangeHandler(e)} value={requrmentfirst}></textarea>
                                        </fieldset>
                                        <button name="submit" type="submit" style={{ background: '#639e77' }}>Submit</button>
                                    </form>
                                    <form id="contact" onSubmit={this.firstForm} style={{ display: secondCard ? 'block' : 'none' }} >
                                        <fieldset style={{ display: 'none' }}>
                                            <input name="subject" placeholder="subject" subject value="Enquiry from Device/Equipment manufacturers" type="text" />
                                        </fieldset>
                                        <fieldset>
                                            <input name="namefirst" placeholder="Customer Name" onChange={(e) => this.inputChangeHandler(e)} value={namefirst} type="text" />
                                        </fieldset>
                                        <fieldset>
                                            <input placeholder="Contact Number" name="phoneNofirst" type="tel" onChange={(e) => this.inputChangeHandler(e)} value={phoneNofirst} />
                                        </fieldset>
                                        <fieldset>
                                            <input required name="emailfirst" placeholder="Email ID" onChange={(e) => this.inputChangeHandler(e)} value={emailfirst} type="email" />
                                        </fieldset>
                                        {/* <fieldset >
                                        <input name="domain" placeholder="Domain being served" onChange={(e) => this.inputChangeHandler(e)} value={namefirst} type="text" />
                                    </fieldset> */}
                                        <fieldset>
                                            <textarea placeholder="Explain the needs , looking forward to work together. " name="requrmentfirst" onChange={(e) => this.inputChangeHandler(e)} value={requrmentfirst}></textarea>
                                        </fieldset>
                                        <button name="submit" type="submit" style={{ background: '#6c6281' }}>Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="sixthSectionSubBg">
                    <div className="container">
                        <div className="row">
                            <div className='col-12' style={{ textAlign: 'center', height: '100px' }}>

                            </div>
                        </div>
                    </div>
                </section>
                <section id="tryit" className="sevenSectionBg">
                    <div className="container">
                        <p className="tryIdHeadText">Experience the solution - Try It</p>
                        <div className="dashBordStyle">
                            <DashBoard />
                        </div>
                        {/* <p className="desttopNeck"></p>
                        <p className="destopbottom"></p> */}
                    </div>
                </section>
                <section className="sevenSectionSubBg">
                    <div className="container">
                        <div className="row">
                            <div className='col-12' style={{ textAlign: 'center', height: '100px' }}>

                            </div>
                        </div>
                    </div>
                </section>
                <section id="pricing" className="eighthSectionBg">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 eightHeadTag">
                                <span className="PricingHead">Pricing</span>
                            </div>
                            <div className='col-md-6 col-12'>
                                <div>
                                    <div className="priceBox">
                                        <div className="priceBoxHead">Per Parameter</div>
                                        <div className="priceBoxHeadText">
                                            $ 0.80/month<br /><br />

                                        + $150 advance *
                                    </div>
                                    </div>
                                    <div className="priceSubBox">
                                        <p className="priceSubBoxText">
                                            Full access to all the <span><a href="#feature" style={{ textDecoration: 'underline' }} >features</a></span>.<br />
                                        Weekly Status calls.<br />
                                        Support availability<br />
                                        Setup help available<br />
                                        Consultant expertise available<br />
                                        ( charges extra)<br /><br />

                                        * $150 waived of for <br />
                                        parameters > 500
                                    </p>
                                    </div>
                                </div>
                                {/* <div style={{ textAlign: 'center' }}>
                                <button className="signUpBut">Sign Up </button>
                            </div> */}
                            </div>
                            <div className='col-md-6 col-12'>
                                <div>
                                    <div className="priceBox">
                                        <div className="priceBoxHead">Per Machine/Equipment</div>
                                        <div className="priceBoxHeadText">
                                            $ 15/month<br /><br />

                                        + $150 advance *
                                    </div>
                                    </div>
                                    <div className="priceSubBox">
                                        <p className="priceSubBoxText">
                                            Full access to all the <span s><a href="#feature" style={{ textDecoration: 'underline' }} >features</a></span>.<br />
                                        Weekly Status calls.<br />
                                        Support availability<br />
                                        Setup help available<br />
                                        Consultant expertise available<br />
                                        ( charges extra)<br /><br />

                                        * $150 waived of for <br />
                                        Machine/Equipment > 15
                                    </p>
                                    </div>
                                </div>
                                {/* <div style={{ textAlign: 'center' }}>
                                <button className="signUpBut">Sign Up </button>
                            </div> */}
                            </div>
                        </div>

                    </div>

                </section>
                <section className="eighthSectionSubBg">
                    <div className="container">
                        <div className="row">
                            <div className='col-12' style={{ textAlign: 'center', height: '100px' }}>
                                <div style={{ textAlign: 'right' }} onClick={this.statusModal}>
                                    <button className="statusBut" onClick={this.statusModal}>Status</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <footer id="contactUs" className="features footerCss" >
                    <div className="container">
                        <div className="row">
                            <div className="col-md-3">
                                <div className="navbar-brand comLogo">Spectra</div>
                            </div>
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-3 mb-2">
                                        <span className="aboutHeading">Contact</span>
                                    </div>
                                    <div className="col-md-7 mb-2">
                                        <span className="aboutSubHeading">sales@starlly.in / +91-9886506163.</span>
                                    </div>
                                    <div className="col-md-3 mb-2">
                                        <span className="aboutHeading">Address</span>
                                    </div>
                                    <div className="col-md-7 mb-2">
                                        <span className="aboutSubHeading">10, Gandhi Bazaar Main Rd, Gandhi Bazaar, Basavanagudi, Bengaluru, Karnataka 560004.</span>
                                    </div>
                                    <div className="col-md-3 mb-2">
                                        <span className="aboutHeading">Careers</span>
                                    </div>
                                    <div className="col-md-7 mb-2">
                                        <span> <a href="https://in.linkedin.com/company/starlly-solutions-pvt.-ltd."><i class="fa fa-linkedin colorWhite" aria-hidden="true"></i></a> </span>
                                        {/* <span> <a href="#"><i class="fa fa-twitter colorWhite" aria-hidden="true"></i></a> </span> */}
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <button className="footerDemoBut"><a className="footerDemoButAlink" href="https://calendly.com/girish-starlly/spectra-solution-demo?month=2020-09" target="_blank" >Request Demo</a></button>
                            </div>
                        </div>
                    </div>
                </footer>

            </div >
        )
    }
}