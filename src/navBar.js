import React from 'react';
import './css/firstPage.css'
import { Link } from "react-router-dom";
import { Modal, ModalBody } from 'reactstrap';
import StatusModal from './statusModal';

export default class NavBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            moblieNav: false,
            statusModa: false
        }
    }
    moblieNavBar = () => {
        console.log('ji')
        this.setState({
            moblieNav: !this.state.moblieNav
        })
    }
    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    handleScroll = () => {
        if (window.scrollY > 20) {
            document.querySelector(".navbar").className = "navbar navbar-expand-md fixed-top marginAlign scroll";
        } else {
            document.querySelector(".navbar").className = "navbar navbar-expand-md fixed-top marginAlign";
        }
    };
    statusModal = (e) => {
        this.setState({
            statusModa: !this.state.statusModa
        })
    }
    render() {
        const { moblieNav, statusModa } = this.state;
        console.log(this.state.statusModa)
        return (
            <div>
                <Modal className="modal-width" style={{ width: "900px", marginTop: '100px' }} isOpen={statusModa} toggle={this.statusModal} >
                    <ModalBody style={{ borderRadius: '20px' }} className="assignModalBody">
                        <StatusModal toggleCancel={this.statusModal} />
                    </ModalBody>
                </Modal>
                <div className="page-wrapper">
                    <div className="nav-wrapper">
                        <div className="grad-bar"></div>
                        {/* navbar-expand-md navbar-dark fixed-top bg-white */}
                        <div >
                            <nav className="navbar navbar-expand-md fixed-top marginAlign">
                                {/* <img src="https://storage.googleapis.com/crmdevelopment/Organisation/ProfilePic/2020/07/31/starlly_logo.png" alt="Company Logo" /> */}
                                <span ><a href="/" className="navbar-brand comLogo">Spectra</a></span>
                                <div className={moblieNav === true ? "menu-toggle is-active" : "menu-toggle"} onClick={this.moblieNavBar} id="mobile-menu">
                                    <span className="bar"></span>
                                    <span className="bar"></span>
                                    <span className="bar"></span>
                                </div>
                                <ul className={moblieNav === true ? "nav mobile-nav" : "nav"} onClick={this.moblieNavBar}>
                                    <li className="nav-item"><a href="#useCases" >Use Cases</a></li>
                                    <li className="nav-item"><a href="#feature">Features </a></li>
                                    <li className="nav-item"><a href="#pricing">Pricing</a></li>
                                    <li className="nav-item"><a href="#tryit">Try it</a></li>
                                    <li className="nav-item"><a href="#status" onClick={this.statusModal}>Status </a></li>

                                </ul>
                                <div className="flex">
                                    <span className="navbutt mr-3"> <a href="#partner">Be a Partner </a></span>
                                    <span className="navbutt"><a className="getStartBut" href="https://calendly.com/girish-starlly/spectra-solution-demo?month=2020-09" target="_blank" >Request Demo</a></span>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}