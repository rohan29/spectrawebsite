import React from 'react';
import Spectra from './photos/spectra.mp4';
import './css/firstPage.css'

export default class VideoModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <div>
                <i class="fas fa-times ml-1" style={{ color: 'gray', float: 'right', paddingBottom: '10px' }} onClick={() => this.props.toggleCancel(false)}></i>

                {/* <div className="embed-responsive embed-responsive-21by9 videoCss">
                    <video width="640" height="480" className="videoTag" loop="" controls autoPlay muted>
                        <source type="video/mp4" src={Spectra} />
                    </video>
                </div> */}
                <div>
                <video className="videoControl"  loop="" controls autoPlay muted>
                        <source type="video/mp4" src={Spectra} />
                    </video>
                </div>
                {/* <span className="laptopbottom"></span> */}
                {/* <video width="640" height="480" className="videoTag" loop="" controls autoPlay muted>
                    <source type="video/mp4" src={Spectra} />
                </video> */}
            </div>
        )
    }
}