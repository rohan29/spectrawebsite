import React from 'react'
import './css/firstPage.css'

export default class FaqSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <div>
                <div>
                    <i class="fas fa-times ml-1" style={{ color: 'gray', float: 'right', paddingBottom: '10px' }} onClick={() => this.props.toggleCancel(false)}></i>
                </div>
                <div className="row">
                    <div className="col-6">
                        <div className="faqCss" style={{ textAlign: 'center', marginBottom: '20px' }}>FAQ</div>
                        <div className="faqCss">Does Spectra work with hardware vendors?</div>
                        <div className="faqCss">Does Spectra provide customer support?</div>
                        <div className="faqCss">Does Spectra charges include unlimited
users and machines support?</div>
                        <div className="faqCss">Is it one time payment or recurring?</div>
                        <div className="faqCss">What kind of machines/equipments are
supported?</div>
                        <div className="faqCss">
                            Can we use it on need basis only?</div>
                        <div className="faqCss">Is my data secure, what are the steps
 taken?</div>
                        <div className="faqCss">Can I integrate Spectra with other 3rd party
 solutions?</div>
                        <div className="faqCss"> Can the data from Spectra be exported?</div>
                        <div className="faqCss">Can customised workflow be built with
 Spectra?</div>

                    </div>
                    <div className="col-6">
                        <div className="faqtextBoxCss">
                            <textarea style={{ height: '200px' }} placeholder="How can we help you?"></textarea>
                        </div>
                        <div style={{ textAlign: 'center' }}>
                            <button className="faqtextBoxButCss">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}